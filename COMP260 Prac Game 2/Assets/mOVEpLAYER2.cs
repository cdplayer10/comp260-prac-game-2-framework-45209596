﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class mOVEpLAYER2 : MonoBehaviour {
	public float speed = 20f;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		GetComponent<Rigidbody>().useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {


	}
	void FixedUpdate () {
		float forwards = Input.GetAxis ("Vertical");
		float HorizontalMove = Input.GetAxis ("Horizontal");
		float xMove = 0;
		float zMove = 0;
		if (forwards > 0) {
			zMove = 1;
	}
		if (forwards<0){
			zMove = -1;
		}
		if (HorizontalMove < 0) {
			xMove = -1;
		}
		if (HorizontalMove > 0) {
			xMove = 1;
		}
		Vector3 pos = rigidbody.position;
		pos.x = pos.x + xMove;
		pos.z = pos.z + zMove;
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		//Debug.Log (Time.fixedDeltaTime);

		// scale the velocity down appropriately
		vel = vel * distToTarget / 1;



		rigidbody.velocity = vel;	
	}
}
