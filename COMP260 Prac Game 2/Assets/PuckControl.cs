﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {
	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();

	}

	void OnCollisionEnter(Collision collision) {
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
			audio.PlayOneShot(wallCollideClip);
		}

	}


	// Use this for initialization



	// Update is called once per frame
	void Update () {
		
	}
	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}

}
