﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {

	static private Scorekeeper instance;
	static public Scorekeeper Instance {
		get { return instance; }
	}

	public int pointsPerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public void OnScoreGoal(int player) {
		score[player] += pointsPerGoal;
		scoreText [player].text = score [player].ToString ();
		if (score [player] >= 10) {
			scoreText [2].text = "Player " + player + " wins";
		}
	}

	void update() {
	}
	void Start () {
		if (instance == null) {
			// save this instance
			instance = this;
		} else {
			// more than one instance exists
			Debug.LogError(
				"More than one Scorekeeper exists in the scene.");
		}  
		for (int i = 0; i <score.Length; i++){
			score [i] = 0;
			scoreText[i].text = "0";
		}
	}    
}
