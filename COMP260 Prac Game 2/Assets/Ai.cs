﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ai : MonoBehaviour {
	public Transform startingPos;
	public Transform endingPos;
	public float speed = 10f;
	private Rigidbody rigidbody;
	private float counter = 11;
	private float xMove = 0;
	private float zMove = 0;
	public Rigidbody targetObject;
	public Rigidbody center;
	private bool reset = true;

	//public Transform target;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		GetComponent<Rigidbody>().useGravity = false;
	}
	void FixedUpdate () {
		Rigidbody target = targetObject;
		float forwards = Input.GetAxis ("Vertical");
		float HorizontalMove = Input.GetAxis ("Horizontal");



		Debug.Log (counter);
		if (rigidbody.position.z > startingPos.position.z) {
			zMove = -1;

		}
		if (rigidbody.position.z < endingPos.position.z) {
			zMove = 1;

		}
	//	if (forwards<0){
	//		zMove = -1;
	//	}
	//	if (HorizontalMove < 0) {
	//		xMove = -1;
	//	}
	//	if (HorizontalMove > 0) {
	//		xMove = 1;
	//	}
		Vector3 pos = rigidbody.position;
		pos.x = pos.x + xMove;
		pos.z = pos.z + zMove;
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;
		//float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		//Debug.Log (Time.fixedDeltaTime);

		// scale the velocity down appropriately


		if (reset == false) {
			target = center;
			Vector3 targetVector = target.position;
			dir = targetVector - rigidbody.position;
			vel = dir.normalized * speed;
			//	float move = speed * Time.fixedDeltaTime;
			distToTarget = dir.magnitude;
			target = targetObject;
		}
		if ((rigidbody.position - center.position).magnitude < 0.5) {
			reset = true;

		}


		Debug.Log ((rigidbody.position - center.position).magnitude);

		if ((center.position - rigidbody.position).magnitude < 3 && (target.position - rigidbody.position).magnitude < 2) {
			Vector3 targetVector = target.position;
			dir = targetVector - rigidbody.position;
			vel = dir.normalized * speed;
		//	float move = speed * Time.fixedDeltaTime;
	 		distToTarget = dir.magnitude;
			reset = false;
		}
		vel = vel * distToTarget / 1;
		rigidbody.velocity = vel;
	}
	// Update is called once per frame
	void Update () {
		
	}
}
